# Unsupervised Face-to-Manga Translation with CycleGAN

Contents of this repo:

- [preprocess.py](./preprocess.py): data preprocessing script.

How to run:

```python
import * from preprocess

celebA_source_path = ...
celebA_dest_path = ...

manga109_source_path = ...
manga109_dest_path = ...

preprocessCelebA(celebA_source_path, dest_path)
preprocessManga(manga109_source_path, manga109_dest_path)
```

- [neural-style-transfer](./neural-style-transfer)

Forked from [fast-neural-style](https://github.com/pytorch/examples/tree/master/fast_neural_style) contains neural style transfer module.

- [cycleGAN](./cyclegan)

Existing implemention of CycleGAN forked from [CycleGAN and pix2pix in PyTorch](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix)
Implemented new dataloader and modified the training pipeline and models.

- [evaluation](./evaluation)
Contains evaluation scripts for Inception Score and FID.

- [sampleData](./sampleData)
Contains sample images from CelebA and manga109.