# Evaluation

Inception score implementation adapted from [Inception Score Pytorch](https://github.com/sbarratt/inception-score-pytorch).
FID score implementation adapted from [Fréchet Inception Distance (FID score) in PyTorch](https://github.com/mseitzer/pytorch-fid).
Anime Face classifier weights adapted from [Anime Face Classifier using Keras](https://github.com/abars/AnimeFaceClassifier).

How to run:

```python
python evaluate.py --real-path </path/to/real/images> --generated-path </path/to/generated/images> --batch-size=4 -c GPU
```