import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def convert_to_pd():
    with open('result.txt', 'r') as f:
        data = f.readlines()

    # first: neural style transfer
    # data = data[1:]
    exp_name_indexes = [i + 4 for i, x in enumerate(data) if x.strip() == 'latest']
    exp_name_indexes.insert(0, 0)

    df = []
    for i in range(0, len(exp_name_indexes) - 1):
        si = exp_name_indexes[i]
        ei = exp_name_indexes[i + 1]

        name = data[si].strip()

        for l in range(si + 1, ei, 4):
            if data[l].strip() == 'latest':
                continue
            # print(data[l])
            epoch = int(data[l])
            res = [float(x) for x in data[l + 3].split()]
            is_mean = res[0]
            is_stdev = res[1]
            fid = res[2]

            row = [name, epoch, is_mean, is_stdev, fid]
            df.append(row)

    df = pd.DataFrame(df, columns=['exp', 'epoch', 'is_mean', 'is_stdev', 'fid'])

    sample = df.groupby('exp').size().reset_index(name='count').sort_values('count')

    print(sample)
    df.to_pickle('result.pkl')

def get_max():
    df = pd.read_pickle('result.pkl')
    exps = df['exp'].drop_duplicates()

    for e in exps:
        print(e)
        cdf = df[df.exp == e]
        print(cdf.sort_values(['is_mean', 'epoch'], ascending=False)[['epoch', 'is_mean', 'is_stdev', 'fid']].head())
        print(cdf.sort_values(['fid', 'epoch'])[['epoch', 'is_mean', 'is_stdev', 'fid']].head())

        print(cdf.sort_values(['epoch'], ascending=False)[['epoch', 'is_mean', 'is_stdev', 'fid']].head(n=1))
        print()
    return


def plot_results():
    df = pd.read_pickle('result.pkl')
    exps = df['exp'].drop_duplicates()
    labels = []

    prefix = 'grayscale_'
    exps = ['rescale64', 'rescale64_grayscale_manga']
    labels = ['color', 'grayscale']
    # prefix = 'transform_'
    # exps = ['rescale64_grayscale_no_transform', 'rescale64_grayscale_manga']
    # labels = ['no transform', 'random horizontal flip']
    prefix = 'lr_'
    exps = ['rescale64_grayscale_manga', 'rescale64_grayscale_no05dim_', 'rescale64_grayscale_2dim_']
    labels = ['0.5x', '1x', '2x']

    '''
    rescale64
    rescale64_grayscale_manga
    rescale64_grayscale_no_transform
    rescale64_grayscale_no05dim_
    rescale64_grayscale_2dim_

    grayscale_no05dim_unet
    grayscale_no2dim_unet
    grayscale_no3dim_unet_
    rescale64_grayscale_dpixel_no05dim_
    rescale64_grayscale_no05dim_resnet6
    '''

    is_mean = plt.figure().add_subplot(111)
    is_stdev = plt.figure().add_subplot(111)
    fid = plt.figure().add_subplot(111)

    for i, e in enumerate(exps):
        cdf = df[df.exp == e]
        lbl = e
        if len(labels) > 0:
            lbl = labels[i]
        is_mean.plot(cdf['epoch'], cdf['is_mean'], label=lbl)
        is_stdev.plot(cdf['epoch'], cdf['is_stdev'], label=lbl)
        fid.plot(cdf['epoch'], cdf['fid'], label=lbl)

    is_mean.legend(loc='upper left')
    is_mean.set_xlabel('epoch')
    is_mean.set_ylabel('mu(IS)')
    is_mean.get_figure().savefig(prefix + 'is_mean.png')

    is_stdev.legend(loc='upper left')
    is_stdev.set_xlabel('epoch')
    is_stdev.set_ylabel('stdev(IS)')
    is_stdev.get_figure().savefig(prefix + 'is_stdev.png')

    fid.legend(loc='upper left')
    fid.set_xlabel('epoch')
    fid.set_ylabel('FID')
    fid.get_figure().savefig(prefix + 'fid.png')



if __name__ == '__main__':
    # convert_to_pd()
    # plot_results()
    get_max()
