#!/bin/bash

#declare -a models=("rescale64_grayscale_manga" "rescale64" "rescale64_grayscale_no05dim_" "rescale64_grayscale_2dim_" "grayscale_no05dim_unet" "grayscale_no2dim_unet" "grayscale_no3dim_unet_" "rescale64_grayscale_dpixel_no05dim_" "rescale64_grayscale_no_transform" "rescale64_grayscale_no05dim_resnet6" "rescale64_grayscale_no05dim_resnet6")

### Invalid
#rescale64_grayscale_nlayer4_spectral
#rescale64_grayscale_nlayer5_spectral
#rescale64_grayscale_nlayer6_spectral
#rescale64_grayscale_nlayer7_spectral

#declare -a models=("rescale64_grayscale_manga" \
#                   "rescale64" \
#                   "rescale64_grayscale_no05dim_" \
#                   "rescale64_grayscale_2dim_" \
#                   "grayscale_no05dim_unet" \
#                   "grayscale_no2dim_unet" \
#                   "grayscale_no3dim_unet_" \
#                   "rescale64_grayscale_dpixel_no05dim_" \
#                   "rescale64_grayscale_no_transform" \
#                   "rescale64_grayscale_no05dim_resnet6" \
#                   "rescale64_grayscale_no05dim_resnet6" \
#                   "rescale64_grayscale_nlayer4_Dspectral" \
#                   "rescale64_grayscale_nlayer4_attn" \
#                   "rescale64_grayscale_nlayer4_Dspectral_attn" \
#                   "rescale64_grayscale_nlayer5" \
#                   "rescale64_grayscale_nlayer5_Dspectral" \
#                   "rescale64_grayscale_nlayer5_attn" \
#                   "rescale64_grayscale_nlayer5_Dspectral_attn" \
#                   "rescale64_grayscale_nlayer6" \
#                   "rescale64_grayscale_nlayer6_Dspectral" \
#                   "rescale64_grayscale_nlayer6_attn" \
#                   "rescale64_grayscale_nlayer6_Dspectral_attn" \
#                   "rescale64_grayscale_nlayer7" \
#                   "rescale64_grayscale_nlayer7_Dspectral" \
#                   "rescale64_grayscale_nlayer7_attn" \
#                   "rescale64_grayscale_nlayer7_Dspectral_attn" \
#                   "rescale256_grayscale_li0_Ddcgan" \
#                   "rescale64_grayscale_spectralonlygen" \
#                   "rescale64_grayscale_Ddcgan_lrd01" \
#                   "rescale64_grayscale_Ddcgan_Dspectral" \
#		           "rescale64_grayscale_dcgan")

declare -a models=("rescale64_grayscale_manga" \
                   "rescale64_grayscale_no_transform" \
                   "rescale64_grayscale_no05dim_" \
                   "rescale64_grayscale_2dim_" \
                   "rescale64_grayscale_Dspectral" \
                   "rescale64_grayscale_attn" \
                   "rescale64_grayscale_nlayer4" \
                   "rescale64_grayscale_nlayer4_Dspectral" \
                   "rescale64_grayscale_nlayer4_attn" \
                   "rescale64_grayscale_nlayer4_Dspectral_attn" \
                   "rescale64_grayscale_nlayer5" \
                   "rescale64_grayscale_nlayer5_Dspectral" \
                   "rescale64_grayscale_nlayer5_attn" \
                   "rescale64_grayscale_nlayer5_Dspectral_attn" \
                   "rescale64_grayscale_nlayer6" \
                   "rescale64_grayscale_nlayer6_Dspectral" \
                   "rescale64_grayscale_nlayer6_attn" \
                   "rescale64_grayscale_nlayer6_Dspectral_attn" \
                   "rescale64_grayscale_nlayer7" \
                   "rescale64_grayscale_nlayer7_Dspectral" \
                   "rescale64_grayscale_nlayer7_attn" \
                   "rescale64_grayscale_nlayer7_Dspectral_attn" \
                   "rescale64_grayscale_nlayer7_rcrop" \
                   "rescale64_grayscale_nlayer7_rcrop_dspectral" \
                   "rescale64_grayscale_nlayer7_rcrop_attn" \
                   "rescale64_grayscale_nlayer7_rcrop_dspectral_attn" \
                   "rescale64_grayscale_dcgan")

for m in "${models[@]}"
do
    echo ${m}

#    for i in `seq 10 10 200`
#    do
#        echo ${i}
#        python -W ignore evaluate.py --real-path=../pytorch-CycleGAN-and-pix2pix/testdata/CelebA/ --generated-path=../pytorch-CycleGAN-and-pix2pix/results/${m}/test_${i}/images/ --batch-size=4 --impl=keras
#    done

#    echo "latest"
    python -W ignore evaluate.py --real-path=../pytorch-CycleGAN-and-pix2pix/testdata/CelebA/ \
    --generated-path=../pytorch-CycleGAN-and-pix2pix/results/${m}/test_latest/images/ --batch-size=16 --impl=keras
done


#python -W ignore evaluate.py --real-path=../pytorch-CycleGAN-and-pix2pix/testdata/CelebA/ --generated-path=../pytorch-CycleGAN-and-pix2pix/results/rescale64_grayscale_manga/test_latest/images/ --batch-size=4 -c GPU --impl=keras

#python -W ignore evaluate.py --real-path=../pytorch-CycleGAN-and-pix2pix/testdata/CelebA/ \
#--generated-path=../pytorch-CycleGAN-and-pix2pix/results/rescale64_grayscale_nlayer4/test_latest/images/ \
#--batch-size=4 --impl=keras

python -W ignore evaluate.py --real-path=../pytorch-CycleGAN-and-pix2pix/testdata/CelebA/ \
    --generated-path=../pytorch-CycleGAN-and-pix2pix/results/rescale64/test_latest/images/ --batch-size=16 --impl=keras