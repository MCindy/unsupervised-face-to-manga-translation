import numpy as np

# import plaidml.keras
# plaidml.keras.install_backend()

import tensorflow as tf
from keras.models import load_model

import torch
from torch import nn
from torch.autograd import Variable
from torch.nn import functional as F
import torch.utils.data
from torchvision.models.inception import inception_v3

from scipy.stats import entropy


def calc_inception_score(preds, N, splits=1):
    # Now compute the mean kl-div
    split_scores = []
    pys = []
    pyxs = []
    for k in range(splits):
        part = preds[k * (N // splits): (k+1) * (N // splits), :]
        py = np.mean(part, axis=0)
        pys.append(py)
        scores = []
        for i in range(part.shape[0]):
            pyx = part[i, :]
            scores.append(entropy(pyx, py))
            pyxs.append(pyx)
        split_scores.append(np.exp(np.mean(scores)))

    return np.mean(split_scores), np.std(split_scores), np.mean(pys), np.mean(pyxs)

def get_keras_model(verbose=False):
    # if(MODEL_HDF5 == 'animeface_vgg16.hdf5'):
    # 	IMAGE_SIZE = 224
    # elif(MODEL_HDF5 == 'animeface_small_cnn.hdf5'):
    # 	IMAGE_SIZE = 32

    MODEL_HDF5 = '../../animeface_vgg16.hdf5'
    # MODEL_HDF5='../../animeface_small_cnn.hdf5'
    with tf.device('/cpu:0'):
        model = load_model(MODEL_HDF5)
        if verbose:
            model.summary()
        return model


def inception_score_keras(imgs, batch_size=32, splits=1):
    """Computes the inception score of the generated images imgs

    imgs -- (3xHxW) numpy images normalized in the range [-1, 1]
    cuda -- whether or not to run on GPU
    batch_size -- batch size for feeding into Inception v3
    splits -- number of splits
    """
    N = len(imgs)
    model = get_keras_model()
    preds = np.zeros((N, 203))

    steps = N // batch_size
    # print(steps)
    for i in range(steps):
        p = model.predict(imgs[i * batch_size: (i+1)*batch_size])
        preds[i * batch_size: (i + 1) * batch_size] = p

    if steps * batch_size < N:
        p = model.predict(imgs[steps * batch_size:])
        preds[steps * batch_size:] = p
    return calc_inception_score(preds, N, splits)


def inception_score(imgs, cuda=True, batch_size=32, resize=False, splits=1):
    """Computes the inception score of the generated images imgs

    imgs -- Torch dataset of (3xHxW) numpy images normalized in the range [-1, 1]
    cuda -- whether or not to run on GPU
    batch_size -- batch size for feeding into Inception v3
    splits -- number of splits
    """
    N = len(imgs)

    assert batch_size > 0
    assert N > batch_size

    # Set up dtype
    if cuda:
        dtype = torch.cuda.FloatTensor
    else:
        if torch.cuda.is_available():
            print("WARNING: You have a CUDA device, so you should probably set cuda=True")
        dtype = torch.FloatTensor

    # Set up dataloader
    dataloader = torch.utils.data.DataLoader(imgs, batch_size=batch_size)

    # Load inception model
    inception_model = inception_v3(pretrained=True, transform_input=False).type(dtype)
    inception_model.eval();
    up = nn.Upsample(size=(299, 299), mode='bilinear').type(dtype)
    def get_pred(x):
        if resize:
            x = up(x)
        x = inception_model(x)
        return F.softmax(x, dim=1).data.cpu().numpy()

    # Get predictions
    preds = np.zeros((N, 1000))

    for i, batch in enumerate(dataloader, 0):
        batch = batch.type(dtype)
        batchv = Variable(batch)
        batch_size_i = batch.size()[0]

        preds[i*batch_size:i*batch_size + batch_size_i] = get_pred(batchv)

    return calc_inception_score(preds, N, splits)
