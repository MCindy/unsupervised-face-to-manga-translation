import os
import glob

from PIL import Image
import xml.etree.ElementTree

imageSize = (128, 128)

def preprocessManga(path='./sampleData/raw/Manga/', savePath='./sampleData/preprocessed/Manga/'):
    for f in glob.glob(path + 'annotations/' + '*.xml'):
        e = xml.etree.ElementTree.parse(f).getroot()
        title = e.attrib['title']
        print(title)

        if not os.path.exists(os.path.join(savePath, title)):
            os.mkdir(os.path.join(savePath, title))

        for page in e.find('pages').iter('page'):
            pageIndex = page.attrib['index']

            faces = page.findall('face')

            # Skip pages with no face
            if len(faces) == 0:
                continue

            imagePath = os.path.join(path, 'images', title, pageIndex.zfill(3) + '.jpg')
            pageImage = Image.open(imagePath)
            for i, face in enumerate(faces):
                xmin = float(face.attrib['xmin'])
                ymin = float(face.attrib['ymin'])
                xmax = float(face.attrib['xmax'])
                ymax = float(face.attrib['ymax'])

                height = ymax - ymin
                width = xmax - xmin

                if (height < 100) or (width < 100):
                    continue

                croppedImage = pageImage.crop((xmin, ymin, xmax, ymax))
                croppedImage = croppedImage.resize(imageSize, Image.BILINEAR)
                saveLocation = os.path.join(savePath, title, pageIndex.zfill(3) + '_' + str(i) + '.jpg')
                croppedImage.save(saveLocation)
    return


def preprocessCelebA(path='./sampleData/raw/CelebA/', savePath='./sampleData/preprocessed/CelebA/'):
    for f in glob.glob(path + '*.jpg'):
        imageName = f.split('/')[-1]
        numImage = int(imageName.split('.')[0])
        if numImage > 3000:
            continue
        # if numImage < 200435:
        #     continue
        # if numImage > 202599:
        #     continue
        print(imageName)

        faceImage = Image.open(f)
        width, height = faceImage.size

        ymin = int(round((height - 110)/2.))
        xmin = int(round((width - 108)/2.))

        croppedImage = faceImage.crop((xmin, ymin, xmin + 108, ymin + 140))
        croppedImage = croppedImage.resize(imageSize, Image.BILINEAR)

        saveLocation = os.path.join(savePath, imageName)
        croppedImage.save(saveLocation)
    return


def preprocessSimpson(path='./sampleData/raw/Simpson/', savePath='./sampleData/preprocessed/Simpson/'):
    for f in glob.glob(path + '*.png'):
        imageName = f.split('/')[-1]
        print(imageName)

        faceImage = Image.open(f)
        croppedImage = faceImage.resize(imageSize, Image.BILINEAR)

        saveLocation = os.path.join(savePath, imageName)
        croppedImage.save(saveLocation)
    return

#preprocessManga('../dataset/Manga109_2017_09_28/', '../preprocessed/Manga109')
preprocessCelebA('../dataset/img_align_celeba/', '../preprocessed/CelebA-larger')
#preprocessSimpson('../dataset/cropped_simpson/', '../preprocessed/Simpson')
