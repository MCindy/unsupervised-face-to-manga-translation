import glob
import shutil

images = list(glob.glob('../preprocessed/CelebA/*.jpg'))
#images = images[:5000]

#for img in images:
#    shutil.copy(img, '../preprocessed/CelebA-copy/img/')

images = images[-2000:]
for img in images:
    shutil.copy(img, './pytorch-CycleGAN-and-pix2pix/testdata/CelebA/')