import time
from options.train_options import TrainOptions
# from data import CreateDataLoader
from dataset import ImageDataset
from models import create_model
from util.visualizer import Visualizer
from PIL import Image

import torch
import torchvision.transforms as transforms


if __name__ == '__main__':
    opt = TrainOptions().parse()
    transforms_ = []
    if opt.grayscale:
        transforms_.append(transforms.Grayscale())
    if opt.random_crop:
        transforms_.append(transforms.RandomResizedCrop(opt.loadSize))
    else:
        transforms_.append(transforms.Resize(opt.loadSize, Image.BICUBIC))
    transforms_.extend([transforms.RandomHorizontalFlip(),
                        transforms.ToTensor(),
                        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    # Configure data loader
    custom_dataset = ImageDataset(target_dataset='Manga', folder_path=opt.dataroot, transforms_=transforms_)
    dataset = torch.utils.data.DataLoader(
                    custom_dataset,
                    batch_size=opt.batch_size,
                    shuffle=not opt.serial_batches,
                    num_workers=int(opt.num_threads))

    dataset_size = len(dataset)
    print('#training images = %d' % dataset_size)

    model = create_model(opt)
    model.setup(opt)
    visualizer = Visualizer(opt)
    total_steps = 0

    for epoch in range(opt.epoch_count, opt.niter + opt.niter_decay + 1):
        epoch_start_time = time.time()
        iter_data_time = time.time()
        epoch_iter = 0

        for i, data in enumerate(dataset):
            iter_start_time = time.time()
            if total_steps % opt.print_freq == 0:
                t_data = iter_start_time - iter_data_time
            visualizer.reset()
            total_steps += opt.batch_size
            epoch_iter += opt.batch_size
            model.set_input(data)
            model.optimize_parameters()

            if total_steps % opt.display_freq == 0:
                save_result = total_steps % opt.update_html_freq == 0
                visualizer.display_current_results(model.get_current_visuals(), epoch, save_result)

            if total_steps % opt.print_freq == 0:
                losses = model.get_current_losses()
                t = (time.time() - iter_start_time) / opt.batch_size
                visualizer.print_current_losses(epoch, epoch_iter, losses, t, t_data)
                if opt.display_id > 0:
                    visualizer.plot_current_losses(epoch, float(epoch_iter) / dataset_size, opt, losses)

            if total_steps % opt.save_latest_freq == 0:
                print('saving the latest model (epoch %d, total_steps %d)' %
                      (epoch, total_steps))
                model.save_networks('latest')

            iter_data_time = time.time()
        if epoch % opt.save_epoch_freq == 0:
            print('saving the model at the end of epoch %d, iters %d' %
                  (epoch, total_steps))
            model.save_networks('latest')
            model.save_networks(epoch)

        print('End of epoch %d / %d \t Time Taken: %d sec' %
              (epoch, opt.niter + opt.niter_decay, time.time() - epoch_start_time))
        model.update_learning_rate()
