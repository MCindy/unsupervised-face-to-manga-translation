# CycleGAN

- Forked from [CycleGAN and pix2pix in PyTorch](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix).

- Only contains relevant code for this project.

- Follows the same train and test command as in the original repo with additional training arguments.

## Train

```python
python train.py --dataroot=</path/to/train/data> --grayscale --loadSize --epoch=200 --use_attn --normD=spectral
```

## Test

```python
python test.py --dataroot=</path/to/test/data> --grayscale --model=cycle_gan --name=<experiment_name> --loadSize=64
```

## Additional training arguments

- `--normD`: normalization type for the discriminator. (instance (default), batch, spectral)

- `--normG`: normalization type for the generator. (instance (default), batch, spectral)

- `--use_attn`: add self-attention modules to the generator and discriminator.

- `--random_crop`: randomly crop the images.

- `--grayscale`: convert the input images into grayscale.

- `--lrD_multiplier`: use `lrD_multiplier` * `lr` for the learning rate of the discriminator.