#!/bin/sh

# rescale64_grayscale_manga
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_manga \
#    --loadSize=64 --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_manga \
#--loadSize=64
#
#
## rescale64
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --model=cycle_gan --name=rescale64 \
#    --loadSize=64 --epoch=${i}
#done
#
python test.py --dataroot=testdata/ \
--model=cycle_gan --name=rescale64 \
--loadSize=64
#
#
## rescale64_grayscale_no05dim_
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_no05dim_ \
#    --loadSize=64 --lrD_multiplier=1 --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_no05dim_ \
#--loadSize=64 --lrD_multiplier=1
#
#
## rescale64_grayscale_2dim_
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_2dim_ \
#    --loadSize=64 --lrD_multiplier=2 --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_2dim_ \
#--loadSize=64 --lrD_multiplier=2
#
#
## rescale64_grayscale_no_transform
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_no_transform \
#    --loadSize=64 --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_no_transform \
#--loadSize=64
#
#
## rescale64_grayscale_nlayer4
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer4 \
#    --loadSize=64 --n_layers_D=4 --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer4 \
#--loadSize=64 --n_layers_D=4
#
#
## rescale64_grayscale_nlayer4_spectral
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer4_spectral \
#    --loadSize=64 --n_layers_D=4 --norm=spectral --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer4_spectral \
#--loadSize=64 --n_layers_D=4 --norm=spectral
#
#
## rescale64_grayscale_nlayer4_Dspectral
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer4_Dspectral \
#    --loadSize=64 --n_layers_D=4 --normD=spectral --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer4_Dspectral \
#--loadSize=64 --n_layers_D=4 --normD=spectral
#
#
## rescale64_grayscale_nlayer4_attn
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer4_attn \
#    --loadSize=64 --n_layers_D=4 --use_attn --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer4_attn \
#--loadSize=64 --n_layers_D=4 --use_attn
#
#
## rescale64_grayscale_nlayer4_Dspectral_attn
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer4_Dspectral_attn \
#    --loadSize=64 --n_layers_D=4 --normD=spectral --use_attn --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer4_Dspectral_attn \
#--loadSize=64 --n_layers_D=4 --normD=spectral --use_attn
#
#
## rescale64_grayscale_nlayer5
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer5 \
#    --loadSize=64 --n_layers_D=5 --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer5 \
#--loadSize=64 --n_layers_D=5


# rescale64_grayscale_nlayer5_spectral
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer5_spectral \
#    --loadSize=64 --n_layers_D=5 --norm=spectral --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer5_spectral \
#--loadSize=64 --n_layers_D=5 --norm=spectral


# rescale64_grayscale_nlayer5_Dspectral
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer5_Dspectral \
#    --loadSize=64 --n_layers_D=5 --normD=spectral --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer5_Dspectral \
#--loadSize=64 --n_layers_D=5 --normD=spectral
#
#
## rescale64_grayscale_nlayer5_attn
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer5_attn \
#    --loadSize=64 --n_layers_D=5 --use_attn --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer5_attn \
#--loadSize=64 --n_layers_D=5 --use_attn
#
#
## rescale64_grayscale_nlayer5_Dspectral_attn
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer5_Dspectral_attn \
#    --loadSize=64 --n_layers_D=5 --normD=spectral --use_attn --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer5_Dspectral_attn \
#--loadSize=64 --n_layers_D=5 --normD=spectral --use_attn
#
#
## rescale64_grayscale_nlayer6
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer6 \
#    --loadSize=64 --n_layers_D=6 --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer6 \
#--loadSize=64 --n_layers_D=6


# rescale64_grayscale_nlayer6_spectral
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer6_spectral \
#    --loadSize=64 --n_layers_D=6 --norm=spectral --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer6_spectral \
#--loadSize=64 --n_layers_D=6 --norm=spectral


# rescale64_grayscale_nlayer6_Dspectral
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer6_Dspectral \
#    --loadSize=64 --n_layers_D=6 --normD=spectral --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer6_Dspectral \
#--loadSize=64 --n_layers_D=6 --normD=spectral


# rescale64_grayscale_nlayer6_attn
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer6_attn \
#    --loadSize=64 --n_layers_D=6 --use_attn --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer6_attn \
#--loadSize=64 --n_layers_D=6 --use_attn


# rescale64_grayscale_nlayer6_Dspectral_attn
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer6_Dspectral_attn \
#    --loadSize=64 --n_layers_D=6 --normD=spectral --use_attn --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer6_Dspectral_attn \
#--loadSize=64 --n_layers_D=6 --normD=spectral --use_attn


# rescale64_grayscale_nlayer7
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7 \
#    --loadSize=64 --n_layers_D=7 --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7 \
#--loadSize=64 --n_layers_D=7


# rescale64_grayscale_nlayer7_spectral
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7_spectral \
#    --loadSize=64 --n_layers_D=7 --norm=spectral --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7_spectral \
#--loadSize=64 --n_layers_D=7 --norm=spectral


# rescale64_grayscale_nlayer7_Dspectral
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7_Dspectral \
#    --loadSize=64 --n_layers_D=7 --normD=spectral --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7_Dspectral \
#--loadSize=64 --n_layers_D=7 --normD=spectral


# rescale64_grayscale_nlayer7_attn
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7_attn \
#    --loadSize=64 --n_layers_D=7 --use_attn --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7_attn \
#--loadSize=64 --n_layers_D=7 --use_attn


# rescale64_grayscale_nlayer7_Dspectral_attn
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7_Dspectral_attn \
#    --loadSize=64 --n_layers_D=7 --normD=spectral --use_attn --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7_Dspectral_attn \
#--loadSize=64 --n_layers_D=7 --normD=spectral --use_attn



# rescale256_grayscale_li0_Ddcgan
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale256_grayscale_li0_Ddcgan \
#    --loadSize=64 --netD=dcgan --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale256_grayscale_li0_Ddcgan \
#--loadSize=64 --netD=dcgan


# rescale64_grayscale_spectralonlygen
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_spectralonlygen \
#    --loadSize=64 --normG=spectral --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_spectralonlygen \
#--loadSize=64 --normG=spectral --epoch=${i}


# rescale64_grayscale_Ddcgan_lrd01
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_Ddcgan_lrd01 \
#    --loadSize=64 --netD=dcgan --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_Ddcgan_lrd01 \
#--loadSize=64 --netD=dcgan


# rescale64_grayscale_Ddcgan_Dspectral
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_Ddcgan_Dspectral \
#    --loadSize=64 --netD=dcgan --normD=spectral --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_Ddcgan_Dspectral \
#--loadSize=64 --netD=dcgan --normD=spectral


# rescale64_grayscale_dcgan
#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ \
#    --grayscale --model=cycle_gan --name=rescale64_grayscale_dcgan \
#    --loadSize=64 --netD=dcgan --normD=spectral --epoch=${i}
#done
#
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_dcgan \
#--loadSize=64 --netD=dcgan --normD=spectral



# rescale64_grayscale_Dspectral
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_Dspectral \
#--loadSize=64 --normD=spectral
#
#
## rescale64_grayscale_attn
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_attn \
#--loadSize=64 --use_attn
#
#
## rescale64_grayscale_nlayer7_rcrop
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7_rcrop \
#--loadSize=64 --n_layers=7
#
## rescale64_grayscale_nlayer7_rcrop_dspectral
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7_rcrop_dspectral \
#--loadSize=64 --n_layers=7 --normD=spectral
#
## rescale64_grayscale_nlayer7_rcrop_attn
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7_rcrop_attn \
#--loadSize=64 --n_layers=7 --use_attn
#
## rescale64_grayscale_nlayer7_rcrop_dspectral_attn
#python test.py --dataroot=testdata/ \
#--grayscale --model=cycle_gan --name=rescale64_grayscale_nlayer7_rcrop_dspectral_attn \
#--loadSize=64 --n_layers=7 --normD=spectral --use_attn

#for i in `seq 10 10 200`
#do
#    python test.py --dataroot=testdata/ --grayscale --model=cycle_gan --name=rescale64_grayscale_no05dim_resnet6 --loadSize=64 --netG=resnet_6blocks --epoch=${i}
#done
