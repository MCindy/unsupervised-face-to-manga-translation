import os
import glob
import random

import numpy as np
from PIL import Image
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms

class ImageDataset(Dataset):
    def __init__(self, target_dataset, folder_path='../sampleData/preprocessed/', transforms_=None):
        if target_dataset == 'Simpson':
            img_format = '/*.png'
        else:
            img_format = '/*.jpg'

        self.source_image_paths = np.asarray(list(glob.glob(os.path.join(folder_path, 'CelebA') + '/*.jpg')))
        if target_dataset == 'Simpson':
            self.target_image_paths = np.asarray(list(glob.glob(os.path.join(folder_path, target_dataset) + img_format)))
        else:
            self.target_image_paths = np.asarray(list(glob.glob(os.path.join(folder_path, target_dataset, '*') + img_format)))


        self.data_len = len(self.source_image_paths)

        self.transform = transforms.Compose(transforms_)

    def __getitem__(self, index):
        src_image_path = self.source_image_paths[index % len(self.source_image_paths)]
        target_image_path = self.target_image_paths[random.randint(0, len(self.target_image_paths) - 1)]

        src_img = Image.open(src_image_path)
        target_img = Image.open(target_image_path)

        src_img = self.transform(src_img)
        target_img = self.transform(target_img)

        return {'A': src_img, 'B': target_img, 'A_paths': src_image_path, 'B_paths': target_image_path}

    def __len__(self):
        return self.data_len
